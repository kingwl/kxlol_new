#include "MagiciteRayCallBack.h"

MagiciteRayCallBack::MagiciteRayCallBack()
{

}

MagiciteRayCallBack::MagiciteRayCallBack(const CallBackFunc &func)
{
    setCallBack(func);
}

float32 MagiciteRayCallBack::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
{
    return _RayCallBack(fixture, point, normal, fraction);
}

void MagiciteRayCallBack::setCallBack(const CallBackFunc &func)
{
    _RayCallBack = func;
}