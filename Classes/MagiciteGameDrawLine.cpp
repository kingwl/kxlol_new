#include "MagiciteGameDrawLine.h"

USING_NS_CC;

MagiciteGameDrawLine::MagiciteGameDrawLine()
{

}

MagiciteGameDrawLine::~MagiciteGameDrawLine()
{

}

void MagiciteGameDrawLine::setColor(const Color4F& color)
{
    _color = color;
}

Color4F MagiciteGameDrawLine::getColor() const
{
    return _color;
}

void MagiciteGameDrawLine::setWidth(float width)
{
    _width = width;
}

float MagiciteGameDrawLine::getWidth() const
{
    return _width;
}

void MagiciteGameDrawLine::setPointA(const Vec2& point)
{
    _from = point;
}

Vec2 MagiciteGameDrawLine::getPointA() const
{
    return _from;
}

void MagiciteGameDrawLine::setPointB(const Vec2& point)
{
    _to = point;
}

Vec2 MagiciteGameDrawLine::getPointB() const
{
    return _to;
}

bool MagiciteGameDrawLine::operator== (const MagiciteGameDrawLine& line) const
{
    if (_from == line._from && _to == line._to) return true;
    return false;
}

MagiciteGameDrawLine* MagiciteGameDrawLine::create(
    const cocos2d::Vec2& pointA,
    const cocos2d::Vec2& pointB,
    float width,
    const cocos2d::Color4F& color)
{
    auto ptr = new MagiciteGameDrawLine();
    if (ptr)
    {
        ptr->setPointA(pointA);
        ptr->setPointB(pointB);
        ptr->setWidth(width);
        ptr->setColor(color);

        return ptr;
    }
    else
    {
        CC_SAFE_DELETE(ptr);
        return nullptr;
    }
}