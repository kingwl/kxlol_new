#ifndef __MAGICITE_RAY_CALLBACK__
#define __MAGICITE_RAY_CALLBACK__

#include <functional>
#include "Box2D/Box2D.h"

class MagiciteRayCallBack : public b2RayCastCallback
{
public:
    typedef std::function<float(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)> CallBackFunc;

    MagiciteRayCallBack();
    MagiciteRayCallBack(const CallBackFunc &func);
    
    float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction);

    void setCallBack(const CallBackFunc &func);
private:
    CallBackFunc                _RayCallBack;

};

#endif //__MAGICITE_RAY_CALLBACK__